﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.ViewModels
{
    public class AddPatientVM
    {
        public string Name { get; set; }
        public int DiagnosisId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.ViewModels
{
    public class DiagnosisVM
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}

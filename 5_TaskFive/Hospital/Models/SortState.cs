﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Models
{
    public enum SortState
    {
        NameAsc,
        NameDesc,
        DiagnAsc,
        DiagnDesc,
        DateAsc,
        DateDesc
    }
}

﻿using System;
using System.Collections.Generic;

namespace Task_2
{
    public partial class Patient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Diagnosis { get; set; }
    }
}
